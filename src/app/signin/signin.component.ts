import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(public authService:AuthService, private router:Router) { }
email;
password;

  ngOnInit() {
  }
  
  onSubmit(){
    this.authService.signin(this.email,this.password);
    //this.router.navigate(['/books']);
  }

}
