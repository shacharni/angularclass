import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  constructor(private booksservice:BooksService, private router:Router,private route:ActivatedRoute,public authservice:AuthService) { }

  title:string;
  author:string;
  id:string;
  isEdit:boolean=false;
buttonText:string="Add book";
userId:string;
 
ngOnInit() {
this.id=this.route.snapshot.params.id;
this.authservice.user.subscribe(user=>{
  this.userId=user.uid;
  if(this.id){
    this.isEdit=true;
    this.buttonText="Edit";
    this.booksservice.getBook(this.id,this.userId).subscribe(
      book=>{
        console.log(book.author);
        this.author=book.data().author;
       this.title=book.data().title;})
  } 
  
})
 
  
  }
  onSubmit(){
    if(this.isEdit){
      this.booksservice.updateBook( this.userId,this.id,this.title,this.author);
    }
    else{
      this.booksservice.addBook(this.userId,this.title,this.author);
    }
    
    this.router.navigate(['/books']);
  }

}
