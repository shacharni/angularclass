import { ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';



@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
   title:string='Welcome';
  books:any;
  books$:Observable<any>;
  userId:string;

  deleteBook(id:string,userId:string){
    this.bookssrvice.deleteBook(id,this.userId);
  }
  constructor(private bookssrvice:BooksService, public authservice:AuthService) { }

  ngOnInit() {

    /*this.books=this.bookssrvice.getBooks().subscribe(
      (books)=>this.books=books
    );
   /* this.books=this.bookssrvice.getBooks();*/
//this.books$=this.bookssrvice.getBooks();
//this.bookssrvice.addBooks();
this.authservice.user.subscribe(
  user=>{
    this.userId=user.uid;
    this.books$=this.bookssrvice.getBooks(this.userId);
  }
)
  }

}
