import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NAVComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { RouterModule, Routes } from '@angular/router';
import { TempformComponent } from './tempform/tempform.component';
import { FormsModule }   from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AddBookComponent } from './add-book/add-book.component';

import { AngularFireAuth } from '@angular/fire/auth';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';




const appRoutes: Routes = [
  { path: 'books', component: WelcomeComponent },
  { path: 'temperatures/:temp/:city', component: TemperaturesComponent},
 { path: 'tempform', component: TempformComponent},
 {path: 'addBook', component: AddBookComponent},
 {path: 'addBook/:id', component: AddBookComponent},
 { path: 'signup', component: SignupComponent },
 { path: 'signin', component: SigninComponent },
  { path: "",
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  
  declarations: [
    AppComponent,
    WelcomeComponent,
    NAVComponent,
    TemperaturesComponent,
    TempformComponent,
    AddBookComponent,
    SignupComponent,
    SigninComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule, 
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFirestoreModule,

    
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )    
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
