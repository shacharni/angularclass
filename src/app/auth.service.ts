import { logging } from 'protractor';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './interfaces/user';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

 user:Observable<User|null>
 //היוזר מנוהל על ידי הפייר בייס 
  constructor(public afAuth:AngularFireAuth,
              private router:Router) { 

    this.user=this.afAuth.authState;
  }
  signup(email:string,password:string){
    this.afAuth.auth.createUserWithEmailAndPassword(email,password).then( () =>{
      this.router.navigate(['/books']);
    })
  }
  signin(email:string,password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then( () =>{
      this.router.navigate(['/books']);
    })
  }
  logout(){
    this.afAuth.auth.signOut();
  }
}
