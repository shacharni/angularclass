import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

 // books:any=[{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'},{title:'laya in Wonderland', author:'shacharni'}];
 /* getBooks(){
    const booksObservale= new Observable(
      observer=>{ 
        setInterval(
        ()=>observer.next(this.books),5000

      )

    }
    )
return booksObservale;
  }pull books from database*/
  userCollection:AngularFirestoreCollection=this.db.collection('users');
  bookCollaction:AngularFirestoreCollection;

  getBooks(userId:string):Observable<any[]> {
    this.bookCollaction=this.db.collection(`users/${userId}/books`);
    return this.bookCollaction.snapshotChanges().pipe(
      map(
        collection=>collection.map(
          document=>{
            const data=document.payload.doc.data();
            data.id=document.payload.doc.id;
            return data;
          }
        )
      )
    )
    
 //return this.db.collection('books').valueChanges({idField:'id'});

  }
  getBook(id:string,userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get();

  }
  addBook(userId:string,title_input:string, author_input:string){
const book={title:title_input, author:author_input};
//this.db.collection('books').add(book);
this.userCollection.doc(userId).collection('books').add(book);
  }
  deleteBook(id:string ,userId:string,){
this.db.doc(`users/${userId}/books/${id}`).delete();
  }
  updateBook(userId:string,id:string,title_input:string, author_input:string){
    this.db.doc(`users/${userId}/books/${id}`).update({title:title_input, author:author_input});
      }
 /* addBooks(){
    setInterval(()=>this.books.push({title:'a new book',author:'shacharni'}),5000)
  }*/

  /*getBooks(){
    setInterval(()=>this.books,1000);
  }*/

  constructor(private db:AngularFirestore) { }

}
