// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBjncrlxiPi42jvZGnf5azkjw48mIRKga4',
    authDomain: 'angular-e4858.firebaseapp.com',
    databaseURL: 'https://angular-e4858.firebaseio.com',
    projectId: 'angular-e4858',
    storageBucket: 'angular-e4858.appspot.com',
    messagingSenderId: '935392936881',
    appId: '1:935392936881:web:7de537ce48ae73e546b390',
    measurementId: 'G-CBXCNP3MHE'
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
